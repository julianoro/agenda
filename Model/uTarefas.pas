unit uTarefas;

interface

type
  TTarefa = class
  private
    FTitulo, FDescricao : string;
    FStatus :  Integer;
  public
    property Titulo: string read FTitulo write FTitulo;
    property Descricao: string read FDescricao write FDescricao;
    property Status: Integer read FStatus write FStatus;
  end;

implementation

{ TCliente }

end.
